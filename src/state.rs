use std::collections::{HashMap, HashSet};
use std::net::SocketAddr;
use std::sync::Arc;

use tokio::sync::mpsc::{self, channel, Receiver, Sender};

use crate::payload::Payload;
use crate::user::{TxUser, User};

// Lets see how far this crap takes us

pub struct State {
    users: HashMap<SocketAddr, Arc<TxUser>>,
    // NOTE: Weak is not hashable, could use weak-table
    rooms: HashMap<String, HashSet<Arc<TxUser>>>,
    tx: Sender<Payload>,
    rx: Receiver<Payload>,
}

impl State {
    pub fn default() -> Self {
        let (tx, rx) = channel::<Payload>(10);
        Self {
            users: HashMap::default(),
            rooms: HashMap::default(),
            tx,
            rx,
        }
    }

    pub fn get_tx(&self) -> Sender<Payload> {
        self.tx.clone()
    }

    fn register_user(&mut self, user: TxUser) {
        self.users.insert(user.get_identifier(), Arc::new(user));
    }

    fn create_or_join_room(&mut self, room: &str, from: SocketAddr) -> Option<&Arc<TxUser>> {
        if let Some(tx_user) = self.users.get(&from) {
            self.rooms
                .entry(room.to_owned())
                .or_insert_with(HashSet::new)
                .insert(tx_user.clone());
            return Some(tx_user);
        }
        None
    }

    pub fn get_identifiers(&self) -> Vec<SocketAddr> {
        self.users
            .values()
            .map(|user| user.get_identifier())
            .collect()
    }

    pub fn get_rooms(&self) -> Vec<String> {
        self.rooms.keys().map(|name| name.to_owned()).collect()
    }

    pub async fn run(&mut self) -> Result<(), mpsc::error::SendError<Payload>> {
        // Feels like State should't own users and rooms and they should just
        // be stored inside this function
        loop {
            match self.rx.recv().await {
                Some(Payload::Register { addr, tx }) => {
                    self.register_user(TxUser::new(addr, tx));
                }
                Some(Payload::ListUsers { from }) => {
                    if let Some(tx_user) = self.users.get(&from) {
                        // TODO get this working with &str?
                        tx_user
                            .send(Payload::CommandResponse {
                                content: format!(
                                    "Users\n-----\n{}\n",
                                    self.get_identifiers()
                                        .iter()
                                        .map(|e| e.to_string())
                                        .collect::<Vec<String>>()
                                        .join("\n")
                                ),
                            })
                            .await?;
                    }
                }
                Some(Payload::ListRooms { from }) => {
                    if let Some(tx_user) = self.users.get(&from) {
                        // TODO get this working with &str?
                        tx_user
                            .send(Payload::CommandResponse {
                                content: format!(
                                    "Rooms\n-----\n{}\n",
                                    self.get_rooms()
                                        .iter()
                                        .map(|e| e.to_string())
                                        .collect::<Vec<String>>()
                                        .join("\n")
                                ),
                            })
                            .await?;
                    }
                }
                Some(Payload::JoinOrCreateRoom { room, from }) => {
                    // Eh
                    if let Some(tx_user) = self.create_or_join_room(&room, from) {
                        tx_user
                            .send(Payload::CommandResponse {
                                content: format!("Joined room: {}", room),
                            })
                            .await?;
                    };
                }
                Some(Payload::Message { from, to, content }) => match to {
                    Some(_) => unimplemented!("Sending to client not implemented yet"),
                    None => {
                        // Send to all but user that sent the message
                        for (addr, tx_user) in self.users.iter().filter(|&(addr, _)| *addr != from)
                        {
                            println!("Sending to {}", addr);
                            tx_user
                                .send(Payload::Message {
                                    from,
                                    to: to.clone(),
                                    content: content.clone(),
                                })
                                .await?;
                        }
                    }
                },
                _ => (),
            }
        }
    }
}
