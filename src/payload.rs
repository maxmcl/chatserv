use std::fmt;
use std::net::SocketAddr;

use tokio::sync::mpsc;

#[derive(Debug)]
pub enum PayloadError {
    InvalidCommand { content: String },
    InvalidCommandArgument { argument: String },
}

impl std::error::Error for PayloadError {}

impl fmt::Display for PayloadError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            PayloadError::InvalidCommand { content } => {
                write!(f, "Invalid command: {}", content)
            }
            PayloadError::InvalidCommandArgument { argument } => {
                write!(f, "Invalid command argument: {}", argument)
            }
        }
    }
}

// TODO: clean up this enum
pub enum Payload {
    Register {
        addr: SocketAddr,
        tx: mpsc::Sender<Payload>,
    },
    Message {
        from: SocketAddr,
        to: Option<Vec<SocketAddr>>,
        content: String,
    },
    ListUsers {
        from: SocketAddr,
    },
    ListRooms {
        from: SocketAddr,
    },
    JoinOrCreateRoom {
        from: SocketAddr,
        room: String,
    },
    LeaveRoom {
        from: SocketAddr,
        room: String,
    },
    CommandResponse {
        content: String,
    },
}

impl Payload {
    fn is_command(content: &str) -> bool {
        match content.chars().next() {
            Some(val) => val == '/',
            _ => false,
        }
    }
    pub fn from_string(from: SocketAddr, content: String) -> Result<Self, PayloadError> {
        // Serialize string received from socket into Payload::Message
        // TODO: clean up in separate functions
        if Self::is_command(&content) {
            // Commands look like: /<action> <arg>, e.g. /list-users

            match content.chars().skip(1).position(|val| val == ' ') {
                Some(mut n) => {
                    // We skipped the leading slash, take it into account
                    n += 1;
                    if n <= 1 || n >= content.len() {
                        return Err(PayloadError::InvalidCommand { content });
                    }
                    // Command with an input arg -- skip the space
                    let room = content[1 + n..].trim_end().to_string();
                    if room.is_empty() {
                        return Err(PayloadError::InvalidCommandArgument { argument: room });
                    }
                    match &content[1..n] {
                        "join-room" => Ok(Payload::JoinOrCreateRoom { from, room }),
                        "leave-room" => Ok(Payload::LeaveRoom { from, room }),
                        _ => Err(PayloadError::InvalidCommand {
                            content: content[1..n + 1].to_string(),
                        }),
                    }
                }
                None => match content[1..].trim_end() {
                    "list-users" => Ok(Payload::ListUsers { from }),
                    "list-rooms" => Ok(Payload::ListRooms { from }),
                    _ => Err(PayloadError::InvalidCommand {
                        content: content[1..].to_string(),
                    }),
                },
            }
        } else {
            Ok(Payload::Message {
                from,
                content,
                to: None,
            })
        }
    }
}
