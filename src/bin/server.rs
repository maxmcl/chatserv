use chatserv::server::Server;
use chatserv::state::State;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Start task to handle messages sent by clients
    let mut state = State::default();
    let tx_state = state.get_tx();
    tokio::spawn(async move { state.run().await });

    // Start task to accept incoming connections
    let mut server = Server::bind("127.0.0.1:12345".to_owned()).await?;
    server.run(tx_state).await?;

    Ok(())
}
