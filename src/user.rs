use std::hash::{Hash, Hasher};
use std::net::SocketAddr;

use chrono;
use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader};
use tokio::net::TcpStream;
use tokio::sync::mpsc;

use crate::payload::Payload;

pub trait User {
    // TODO: generics
    fn get_identifier(&self) -> SocketAddr;
}

// The sending half of the User
pub struct TxUser {
    addr: SocketAddr,
    tx: mpsc::Sender<Payload>,
}

impl Hash for TxUser {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.addr.hash(state);
    }
}

impl PartialEq for TxUser {
    fn eq(&self, other: &Self) -> bool {
        self.addr == other.addr
    }
}

impl Eq for TxUser {}

// The receiving half of the User
pub struct RxUser {
    addr: SocketAddr,
    socket: TcpStream,
    rx: mpsc::Receiver<Payload>,
    tx: mpsc::Sender<Payload>,
}

impl User for TxUser {
    fn get_identifier(&self) -> SocketAddr {
        self.addr
    }
}

impl User for RxUser {
    fn get_identifier(&self) -> SocketAddr {
        self.addr
    }
}

impl RxUser {
    pub fn new(
        addr: SocketAddr,
        socket: TcpStream,
        rx: mpsc::Receiver<Payload>,
        tx: mpsc::Sender<Payload>,
    ) -> Self {
        Self {
            addr,
            socket,
            rx,
            tx,
        }
    }

    pub async fn run(&mut self) {
        let (reader, mut writer) = self.socket.split();
        let mut reader = BufReader::new(reader);
        let mut buffer = String::with_capacity(1024);
        loop {
            tokio::select! {
                result = reader.read_line(&mut buffer) => {
                    match result {
                        Ok(n) if n == 0 => {
                            println!("Connection reset by peer");
                            break
                        }
                        Ok(n) => {
                            println!("Received: {}", buffer);
                            let message = String::from(&buffer[..n]);
                            match Payload::from_string(self.addr, message) {
                                Ok(payload) => {
                                    if let Err(e) = self.tx.send(payload
                                    ).await {
                                        eprintln!("Error while sending to queue: {}", e);
                                        break
                                    }
                                    }
                                // TODO: report back to client
                                Err(e) => eprintln!("Error while serializing string command: {}", e),
                            }
                            buffer.clear();
                        }
                        Err(e) => {
                            eprintln!("Failed to read from socket; err = {:?}", e);
                            break
                        }
                    };
                }
                payload = self.rx.recv() => {
                    match payload {
                        Some(Payload::Message {content, from, ..}) => {
                            println!("Received message from queue: {:?}", content);
                            if let Err(e) = writer.write_all(
                                Self::format(from, content).as_bytes()).await {
                                eprintln!("Failed to write to socket; err = {:?}", e);
                                break;
                            }
                        }
                        Some(Payload::CommandResponse { content }) => {
                            println!("Received message from queue: {:?}", content);
                            if let Err(e) = writer.write_all(content.as_bytes()).await {
                                eprintln!("Failed to write to socket; err = {:?}", e);
                                break;
                            }
                        }
                        _ => ()
                    }
                }
            }
        }
    }

    fn format(addr: SocketAddr, content: String) -> String {
        format!(
            "{} | {} > {}",
            chrono::offset::Local::now().format("%Y-%m-%d %H:%M:%S"),
            addr,
            content
        )
    }
}

impl TxUser {
    pub fn new(addr: SocketAddr, tx: mpsc::Sender<Payload>) -> Self {
        Self { addr, tx }
    }

    pub async fn send(&self, payload: Payload) -> Result<(), mpsc::error::SendError<Payload>> {
        self.tx.send(payload).await
    }
}
