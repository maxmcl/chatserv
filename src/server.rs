use std::net::SocketAddr;

use tokio::io;
use tokio::net::TcpListener;
use tokio::sync::mpsc;

use crate::payload::Payload;
use crate::user::RxUser;

pub struct Server {
    listener: TcpListener,
}

impl Server {
    pub async fn bind(socket_addr: String) -> Result<Self, Box<dyn std::error::Error>> {
        let addr = socket_addr.parse::<SocketAddr>()?;
        println!("Awaiting connections on {}", addr);
        let listener = TcpListener::bind(&addr).await?;
        Ok(Self { listener })
    }

    pub async fn run(&mut self, state_tx: mpsc::Sender<Payload>) -> io::Result<()> {
        // Process incoming connections
        loop {
            let (socket, addr) = self.listener.accept().await?;
            println!("Accepted connection from {}", addr);

            let (tx_user, rx_user) = mpsc::channel::<Payload>(10);
            let mut rx_user = RxUser::new(addr, socket, rx_user, state_tx.clone());

            // Register new user into state
            match state_tx.send(Payload::Register { addr, tx: tx_user }).await {
                Ok(_) => {
                    // Start the receiving half of the user
                    tokio::spawn(async move {
                        rx_user.run().await;
                        println!("Connection to {} closed", addr);
                    });
                }
                Err(_) => {
                    eprintln!("Failed to register new user from {}", addr);
                }
            }
        }
    }
}
